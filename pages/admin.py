from django.contrib import admin
from .models import Contact, Event, Notice

# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'phone_no','contact_date')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'email')
    list_per_page = 25

admin.site.register(Contact, ContactAdmin)


class NoticeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'notice_type', 'location','description','notice_date')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'notice_date')
    list_per_page = 25

admin.site.register(Notice, NoticeAdmin)



class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'location', 'description','start_date','end_date')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'start_datetime')
    list_per_page = 25

admin.site.register(Event, EventAdmin)

