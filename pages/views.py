from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.mail import send_mail
from .models import Contact, Event, Notice
from user.models import MemberInfo
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# Create your views here.
def index(request):
    notices = Notice.objects.order_by('-created_date')[:5]
    events = Event.objects.order_by('-created_date')[:3]
    return render(request, 'pages/index.html', {'notices':notices, 'events':events})

def chairman(request):
    return render(request, 'pages/chairman.html')

def mission(request):
    return render(request, 'pages/mission.html')

def goals(request):
    return render(request, 'pages/goals.html')

def programs(request):
    return render(request, 'pages/programs.html')

def program_detail(request):
    return render(request, 'pages/program-detail.html')

def staffs(request):
    staff = MemberInfo.objects.filter(member_type=1)
    paginator = Paginator(staff, 6)
    page = request.GET.get('page')
    paged_listings = paginator.get_page(page)
    staffs = { 'staffs':paged_listings }
    return render(request, 'pages/staff.html', staffs)

def staff(request,id):
    staff = MemberInfo.objects.get(id=id)
    return render(request, 'pages/staff-detail.html',{'staff':staff})

def news(request):
    return render(request, 'pages/news.html')

def news_detail(request):
    return render(request, 'pages/news-detail.html')

def gallery(request):
    return render(request, 'pages/gallery.html')

def downloads(request):
    return render(request, 'pages/index.html')

def contact(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone']
        message = request.POST['message']

        contact = Contact(name=name, email=email, phone_no=phone, message=message)
        contact.save()
        # Send Email
        send_mail(
            'DKMC Website inquiry',
            'There has been inquiry by user ' + name + ' about the college.',
            'info@dkmc.edu.np',
            ['sujil@nsdevil.com'],
            fail_silently = False,
            html_message = '<h3>Deatils about the query:</h3><br>Name: ' + name + '<br>Email: ' + email + '<br>Phone: ' + phone + '<br><br><b>Message:</b><br>' + message
        )
        messages.success(request, "Your message has been sucessfully emailed to our college email address")

        return redirect('contact')
    return render(request, 'pages/contact.html')

def notices(request):
    notice = Notice.objects.order_by('-created_date')
    paginator = Paginator(notice, 6)
    page = request.GET.get('page')
    paged_listings = paginator.get_page(page)
    notices = { 'notices':paged_listings }
    return render(request, 'pages/notice.html', notices)

def notice(request,id):
    notice = Notice.objects.get(id=id)
    return render(request, 'pages/notice-inner.html',{'notice':notice})


def events(request):
    event = Event.objects.order_by('-created_date')
    paginator = Paginator(event, 3)
    page = request.GET.get('page')
    paged_listings = paginator.get_page(page)
    events = { 'events':paged_listings }
    return render(request, 'pages/events.html', events)

def event(request,id):
    event = Event.objects.get(id=id)
    return render(request, 'pages/event-inner.html',{'event':event})