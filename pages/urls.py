from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='homepage'),
    path('chairman', views.chairman, name='chairman'),
    path('mission', views.mission, name='mission'),
    path('goals', views.goals, name='goals'),
    path('programs', views.programs, name='programs'),
    path('program-detail', views.program_detail, name='program-detail'),
    
    path('news', views.news, name='news'),
    path('news-detail', views.news_detail, name='news-detail'),
    path('gallery', views.gallery, name='gallery'),
    path('downloads', views.downloads, name='downloads'),
    path('contact', views.contact, name='contact'),

    path('notices', views.notices, name="notices"),
    path('notice/<int:id>', views.notice, name="notice-inner"),
    
    path('events', views.events, name="events"),
    path('event/<int:id>', views.event, name="event-inner"),

    path('staffs', views.staffs, name='staff'),
    path('staff/<int:id>', views.staff, name='staff-detail'),
]
