from django.db import models
from django.urls import reverse

# Create your models here.
class MemberInfo(models.Model):
    name = models.CharField(max_length=200)
    post = models.CharField(max_length=30)
    member_type = models.IntegerField()
    email = models.EmailField(max_length=100, null=True, blank=True)
    phone_number = models.CharField(max_length=100, null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    photo = models.ImageField(upload_to='faculty_members',blank=True, null=True)
    additional_info = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('user_staff_detail', kwargs={'pk': self.pk})
