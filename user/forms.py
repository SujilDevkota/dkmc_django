from django import forms
from pages.models import Notice, Event
from .models import MemberInfo

class NoticeForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = '__all__'


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields ='__all__'


class MemberInfoForm(forms.ModelForm):
    phone_number = forms.CharField(widget=forms.TextInput(attrs={'type':'tel'}))
    class Meta:
        model = MemberInfo
        fields ='__all__'