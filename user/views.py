from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import messages, auth
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
# from django.utils.decorators import method_decorator
from pages.models import Notice, Event, Contact
from .models import MemberInfo
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from .forms import NoticeForm, EventForm, MemberInfoForm


# Create your views here.
def login(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                # messages.success(request, 'You are now logged in')
                return redirect('dashboard')
            else:
                messages.error(request, 'Invalid Username or Password')
                return redirect('login')
        else:
            return render(request,'user/login.html')
    else:
        return redirect('dashboard')    

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        messages.success(request, 'You are now log out')
        return redirect('login')

def error_404(request, exception):
        data = {}
        return render(request,'user/404.html',data)

def error_500(request):
        data = {}
        return render(request,'user/500.html',data)

@login_required()
def dashboard(request):
    return render(request, 'user/dashboard.html')

class NoticeList(LoginRequiredMixin, ListView):
    model = Notice
    template_name = "user/notice_list.html"

class NoticeDetail(LoginRequiredMixin, DetailView):
    model = Notice
    template_name = "user/notice_detail.html"

class NoticeCreate(LoginRequiredMixin, CreateView):
    model = Notice
    form_class = NoticeForm
    template_name = "user/notice_form.html"

class NoticeUpdate(LoginRequiredMixin, UpdateView):
    model = Notice
    form_class = NoticeForm
    template_name = "user/notice_form.html"

class NoticeDelete(LoginRequiredMixin, DeleteView):
    model = Notice
    success_url = reverse_lazy('user_notice')

# Views for event handling
class EventList(LoginRequiredMixin, ListView):
    model = Event
    template_name = "user/event_list.html"

class EventDetail(LoginRequiredMixin, DetailView):
    model = Event
    template_name = "user/event_detail.html"

class EventCreate(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm
    template_name = "user/event_form.html"

class EventUpdate(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventForm
    template_name = "user/event_form.html"

class EventDelete(LoginRequiredMixin, DeleteView):
    model = Event
    success_url = reverse_lazy('user_event')


# Views for event handling
class ContactList(LoginRequiredMixin, ListView):
    model = Contact
    template_name = "user/contact_list.html"

class ContactDetail(LoginRequiredMixin, DetailView):
    model = Contact
    template_name = "user/contact_detail.html"


# Views for Staff data handling
class StaffList(LoginRequiredMixin, ListView):
    model = MemberInfo
    template_name = "user/staff_list.html"

class StaffDetail(LoginRequiredMixin, DetailView):
    model = MemberInfo
    template_name = "user/staff_detail.html"

class StaffCreate(LoginRequiredMixin, CreateView):
    model = MemberInfo
    form_class = MemberInfoForm
    template_name = "user/staff_form.html"

class StaffUpdate(LoginRequiredMixin, UpdateView):
    model = MemberInfo
    form_class = MemberInfoForm
    template_name = "user/staff_form.html"

class StaffDelete(LoginRequiredMixin, DeleteView):
    model = MemberInfo
    success_url = reverse_lazy('user_staff')