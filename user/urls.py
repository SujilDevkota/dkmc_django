from django.urls import path
from . import views

urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
]

# Urls related with notice
urlpatterns += [
    path('notice', views.NoticeList.as_view(), name='user_notice'),
    path('notice/<int:pk>', views.NoticeDetail.as_view(), name='user_notice_detail'),
    path('notice/create', views.NoticeCreate.as_view(), name='user_notice_create'),
    path('notice/update/<int:pk>', views.NoticeUpdate.as_view(), name='user_notice_update'),
    path('notice/<int:pk>/delete/', views.NoticeDelete.as_view(), name='user_notice_delete'),   
]

# Urls related with events
urlpatterns += [
    path('event', views.EventList.as_view(), name='user_event'),
    path('event/<int:pk>', views.EventDetail.as_view(), name='user_event_detail'),
    path('event/create', views.EventCreate.as_view(), name='user_event_create'),
    path('event/update/<int:pk>', views.EventUpdate.as_view(), name='user_event_update'),
    path('event/<int:pk>/delete/', views.EventDelete.as_view(), name='user_event_delete'),   
]

# Urls related with contact form submission
urlpatterns += [
    path('contact', views.ContactList.as_view(), name='user_contact'),
    path('contact/<int:pk>', views.ContactDetail.as_view(), name='user_contact_detail'),
]

# Urls related with staffs
urlpatterns += [
    path('staff', views.StaffList.as_view(), name='user_staff'),
    path('staff/<int:pk>', views.StaffDetail.as_view(), name='user_staff_detail'),
    path('staff/create', views.StaffCreate.as_view(), name='user_staff_create'),
    path('staff/update/<int:pk>', views.StaffUpdate.as_view(), name='user_staff_update'),
    path('staff/<int:pk>/delete/', views.StaffDelete.as_view(), name='user_staff_delete'),   
]