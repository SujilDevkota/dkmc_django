from django.contrib import admin
from .models import MemberInfo

# Register your models here.
class MemberInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'name','post', 'email', 'phone_number', 'about','created_date','updated_date')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'email')
    list_per_page = 25

admin.site.register(MemberInfo, MemberInfoAdmin)